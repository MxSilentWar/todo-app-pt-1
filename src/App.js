import React, { useState } from "react";
import { todos as todosList } from "./todos.js";
import { v4 as uuid } from 'uuid';

const App = () => {
  const [todos, setTodos] = useState(todosList)
  const [inputText, setInputText] = useState('')

  const handleAddTodo = (event) => {
    if (event.which === 13) {
      const newID = uuid()
      const newTodo = {
          userId: 1,
          id: newID,
          title: inputText,
          completed: false,
      }
      const newTodos = [...todos]
      newTodos.push(newTodo)
      setTodos(newTodos)
      setInputText('')
    }
  }

  const handleToggle = (todoID) => (event) => {
    console.log(`you have clicked ${todoID}`)
    const newList = todos.map((item) => {
      if (item.id === todoID) {
        return Object.assign({}, item, { completed: !item.completed})
      } else {
        return item
      }
    })
    setTodos(newList)
  }

  const handleDelete = (id) => (event) => {
    const newList = todos.filter(item => (item.id !== id))
    setTodos(newList)
  }

  const handleMassDestruction = (event) => {
    const newList = todos.filter(item => (!item.completed))
    setTodos(newList)
  }

    return (
      <section className="todoapp">
        <header className="header">
          <h1>todos</h1>
          <input 
          onChange={(event) => setInputText(event.target.value)} 
          onKeyDown={(event) => handleAddTodo(event)}
          className="new-todo" 
          value={inputText}
          placeholder="What needs to be done?" 
          autoFocus />
        </header>
        <TodoList 
          todo={todos}
          onToggle={handleToggle}
          onDelete={handleDelete}
        />
        <footer className="footer">
          <span className="todo-count">
            <strong>0</strong> item(s) left
          </span>
          <button 
            className="clear-completed"
            onClick={handleMassDestruction}
          >Clear completed</button>
        </footer>
      </section>
    );
}

const TodoList = (props) => {

  return (
    <section className="main">
      <ul className="todo-list">
        {props.todo.map(todo => (
          <TodoItem 
            title={todo.title} 
            completed={todo.completed} 
            key={todo.id} 
            onToggle={props.onToggle(todo.id)}
            onDelete={props.onDelete(todo.id)}
          />
        ))}
      </ul>
    </section>
  );
}

const TodoItem = (props) => {

  return (
    <li className={props.completed ? "completed" : ""}>
      <div className="view">
        <input 
          className="toggle" 
          type="checkbox" 
          checked={props.completed} 
          onChange={props.onToggle}
          />
        <label>{props.title}</label>
        <button 
          className="destroy" 
          onClick={props.onDelete}
        />
      </div>
    </li>
  );
}



export default App;
